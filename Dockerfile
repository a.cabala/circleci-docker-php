FROM cimg/php:7.4
# @see: https://github.com/CircleCI-Public/cimg-php/blob/master/7.4/Dockerfile

RUN NODE_VERSION="12.19.0" && \
	curl -L -o node.tar.xz "https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.xz" && \
	sudo tar -xJf node.tar.xz -C /usr/local --strip-components=1 && \
	rm node.tar.xz && \
	sudo ln -s /usr/local/bin/node /usr/local/bin/nodejs

RUN sudo apt-get update && sudo apt-get install php7.4-xdebug -y &&  sudo apt-get install git &&\
    sudo rm -rf /var/lib/apt/lists/*

CMD ["bash"]
